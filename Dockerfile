FROM ubuntu:16.04
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

# Based on work by Fer Uria - https://github.com/fauria/docker-lamp

RUN apt-get update
RUN apt-get upgrade -y

COPY debconf.selections /tmp/
RUN debconf-set-selections /tmp/debconf.selections

RUN apt-get install -y software-properties-common
RUN LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
RUN apt-get update


RUN apt-get install -y \
# Basic packages
    zip \
	unzip \
	git \
	nano \
	tree \
	vim \
	curl \
	ftp \
# PHP
	php5.6 \
	php5.6-bz2 \
	php5.6-cgi \
	php5.6-cli \
	php5.6-common \
	php5.6-curl \
	php5.6-dev \
	php5.6-enchant \
	php5.6-fpm \
	php5.6-gd \
	php5.6-gmp \
	php5.6-imap \
	php5.6-interbase \
	php5.6-intl \
	php5.6-json \
	php5.6-ldap \
	php5.6-mbstring \
	php5.6-mcrypt \
	php5.6-mysql \
	php5.6-odbc \
	php5.6-opcache \
	php5.6-pgsql \
	php5.6-phpdbg \
	php5.6-pspell \
	php5.6-readline \
	php5.6-recode \
	php5.6-snmp \
	php5.6-sqlite3 \
	php5.6-sybase \
	php5.6-tidy \
	php5.6-xmlrpc \
	php5.6-xsl \
	php5.6-zip \
# Apache
    apache2 \
	libapache2-mod-php5.6 \
# Postfix
    postfix \
# Composer
	composer

RUN composer require zendframework/zendframework

RUN echo "mysql-server mysql-server/root_password password 12345" | debconf-set-selections
RUN echo "mysql-server mysql-server/root_password_again password 12345" | debconf-set-selections
RUN apt-get install mysql-server -y
RUN usermod -d /var/lib/mysql/ mysql
RUN sed -i "s/bind-address/#bind-address/g" /etc/mysql/mysql.conf.d/mysqld.cnf

RUN usermod -u 1000 www-data


ENV LOG_STDOUT **Boolean**
ENV LOG_STDERR **Boolean**
ENV LOG_LEVEL warn
ENV ALLOW_OVERRIDE All
ENV DATE_TIMEZONE UTC
ENV TERM dumb

RUN a2enmod rewrite
RUN chown -R www-data:www-data /var/www/html

VOLUME /var/www/html
VOLUME /var/log/httpd
VOLUME /var/lib/mysql
VOLUME /var/log/mysql
VOLUME /etc/apache2

RUN mkdir /import
VOLUME /import

COPY index.php /var/www/html/
COPY run-lamp.sh /usr/sbin/
RUN chmod +x /usr/sbin/run-lamp.sh


EXPOSE 80
EXPOSE 3306

CMD ["/usr/sbin/run-lamp.sh"]
